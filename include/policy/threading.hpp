#ifndef IDGEN_POLICY_THREADING_HPP_
#define IDGEN_POLICY_THREADING_HPP_
#include <atomic>
namespace idgen
{
namespace policy
{
template<typename T>
class SingleThreaded
{
protected:
  typedef T id_type;
};

template<typename T>
class MultiThreaded
{
protected:
  typedef std::atomic<T> id_type;
};

} // namespace policy
} // namespace idgen
#endif // IDGEN_POLICY_THREADING_HPP
