#ifndef IDGEN_ID_GEN_HPP_
#define IDGEN_ID_GEN_HPP_
#include <functional>
#include <limits>
#include <policy/threading.hpp>
namespace idgen
{
  /*!
   * \brief : Integer ID generator
   *
   * \tparam IntegerType - base type for ID (actual underlying type is defined by threading policy). default - uint64_t
   * \tparam ThreadingPolicy - threading policy type. Must typedef id_type. Default - policy::SingleThreaded
   */
template<
  typename IntegerType = uint64_t,
  template<typename T> class ThreadingPolicy = policy::SingleThreaded
  >
class Factory: public ThreadingPolicy<IntegerType>
{
private:
  typedef ThreadingPolicy<IntegerType> threading_policy_t;
public:
  typedef std::function<bool(IntegerType)> IsFreeCallback;
public:
  const IntegerType INVALID_ID = std::numeric_limits<IntegerType>::max();
public:
  /*!
   * \brief : Constructor
   *
   * \tparam min_id - minimal id value. default - 1
   * \tparam max_id - maximum id value. default - max integer value - 1
   * \tparam is_free - callback for checking if id is free (for multi-threaded use, user must make callback synced himself)
   */
  Factory(
      IntegerType min_id = static_cast<IntegerType>(1),
      IntegerType max_id = std::numeric_limits<IntegerType>::max() - 1,
      IsFreeCallback is_free = [](IntegerType /*n*/) { return true; }
      )
    : last_(min_id-1)
    , min_(min_id)
    , max_(max_id)
    , IsFree_(is_free)
  {
    if(min_ >= max_)
    {
      throw std::runtime_error("Invalid range");
    }
    if(INVALID_ID == max_id)
    {
      throw std::runtime_error("Invalid max value: try to decrease it");
    }
  }
  /*!
   * \brief GetId get new ID. Takes user-provided callback into account (see @constructor for details).
   *
   * \return New ID value or INVALID_ID if no IDs are available.
   */
  IntegerType GetId()
  {
    bool looped = false;
    while(!looped)
    {
      IntegerType id = ++last_;
      if(max_ == id) // loop it
      {
        id = (last_ = min_);
        looped = true;
      }
      if(IsFree_(id))
      {
        return id;
      }
    }
    return INVALID_ID;
  }

private:
  typename threading_policy_t::id_type last_;
  const IntegerType min_;
  const IntegerType max_;
private:
  IsFreeCallback IsFree_;
};
} // namespace idgen
#endif // IDGEN_ID_GEN_HPP_
