#include <id_gen.hpp>
#include <vector>
#include <iostream>

namespace idgen
{
namespace test
{
typedef std::function<bool()> testcase;

bool CheckLoopReuse()
{
  std::cout << __func__ << " ...";
  unsigned min = 1;
  unsigned max = 2;
  idgen::Factory<unsigned> f(min, max);
  unsigned id;
  for(auto i = min; i <= max+1; ++i)
  {
    id = f.GetId();
  }
  return id == min ? true : false;
}

bool CheckLoopNoReuse()
{
  std::cout << __func__ << " ...";
  unsigned min = 1;
  unsigned max = 2;
  idgen::Factory<unsigned> f(min, max, [](unsigned){return false;});
  unsigned id;
  for(auto i = min; i <= max+1; ++i)
  {
    id = f.GetId();
  }
  return id == f.INVALID_ID ? true : false;

}

bool CheckLoopReuse_mt()
{
  std::cout << __func__ << " ...";
  unsigned min = 1;
  unsigned max = 2;
  idgen::Factory<unsigned, idgen::policy::MultiThreaded> f(min, max);
  unsigned id;
  for(auto i = min; i <= max+1; ++i)
  {
    id = f.GetId();
  }
  return id == min ? true : false;
}

bool CheckLoopNoReuse_mt()
{
  std::cout << __func__ << " ...";
  unsigned min = 1;
  unsigned max = 2;
  idgen::Factory<unsigned, idgen::policy::MultiThreaded> f(min, max, [](unsigned){return false;});
  unsigned id;
  for(auto i = min; i <= max+1; ++i)
  {
    id = f.GetId();
  }
  return id == f.INVALID_ID ? true : false;

}
} // namespace test
} // namespace idgen

namespace
{
static const std::vector<idgen::test::testcase> testcases = 
{
  idgen::test::CheckLoopReuse,
  idgen::test::CheckLoopNoReuse,
  idgen::test::CheckLoopReuse_mt,
  idgen::test::CheckLoopNoReuse_mt,
};

void RunTests()
{
  for(const auto t: testcases)
  {
    std::cout << (t() ? "ok" : "failed") << std::endl; 
  }
}
} // anonymous namespace

int main()
{
  RunTests();
}
